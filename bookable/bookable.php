<?php
/**
 * Plugin Name: Bookable
 * Description: Helps to create a bookable product
 * Author: Calvin Rodrigues
 * Version: 1.0.0
 */

//  define('WP_DEBUG', true);

 function register_booking_product_type(){
    class WC_Product_Bookable extends WC_Product{

        public function __construct($product){
            $this->product_type = 'bookable_product';
            parent::__construct($product); 
        }
        public function is_purchasable()
        {
            return true;
        }
        public function is_in_stock()
        {
            return true;
        }

        public function set_id($id)
        {
            $this->id = $id;
        }


        public function get_price($context = 'view')
        {
            $price = get_post_meta($this->id,'_booking_price',true);
            $this->set_prop( 'price', $price);
            return $price;
        }

        

        public function set_regular_price($price)
        {
            $this->set_prop( 'regular_price', wc_format_decimal( 0 ) );
        }

        public function get_price_html( $currency = '')
        {
            if ( '' === $this->get_price() ) {
                $price = apply_filters( 'woocommerce_empty_price_html', '', $this );
            } else {
                $price = wc_price( wc_get_price_to_display( $this ) ) . $this->get_price_suffix();
            }
	
            return apply_filters( 'woocommerce_get_price_html', '', $this );
        }
  }
 }

 function add_new_product_type($product_type){
    $product_type['bookable_product'] = __('Bookable product');
    return $product_type;

}



function booking_product_class( $classname, $product_type ) {

    if ( $product_type == 'bookable_product' ) { 
        $classname = 'WC_Product_Bookable';
    }

    return $classname;
}



function create_bookable_settings_tab($tabs){

    $tabs['bookable_product'] = array(
        'label'  => __('Bookings Settings', 'wcpt'),
        'target' => 'bookable_product_options',
        'class'  => __('show_if_bookable_product'),
    );

    return $tabs;
}

function add_product_panel(){
?>
    <div id='bookable_product_options' class='panel woocommerce_options_panel'>
        <div class='options_group'>
            <?php
                woocommerce_wp_text_input(array(
                        'id'    => '_booking_price',
                        'label' => __('Price per booking', 'wcpt'),
                    ));
            ?>
        </div>
    </div>

    <?php
}


function save_booking_price($field_id){

    if(isset($_POST['_booking_price'])){
        update_post_meta($field_id, '_booking_price', $_POST['_booking_price']);
    }  

}

function create_bookable_product_template(){
    global $product;

    if('bookable_product' == $product->get_type()){
        $template_path = plugin_dir_path(__FILE__) . 'templates/';
        wc_get_template('add-to-cart/booking.php',
        '',
        '',
        trailingslashit($template_path));
    }
}



//handling input from order page
//adding additional note to cart_data 
function add_to_cart_data( $cart_item, $product_id ){

    if( isset( $_POST['additional_note'] ) ) {

        $cart_item['additional_note'] = sanitize_text_field( $_POST['additional_note'] );

    }

    return $cart_item;
}

//displaying additional note on cart page
function display_on_cart( $data, $cart_item ) {

    if ( isset( $cart_item['additional_note'] ) ){

        $data[] = array(

            'name' => 'Additional notes on order',

            'value' => sanitize_text_field( $cart_item['additional_note'] )

        );

    }

    return $data;

}


//adding additional note to order_meta
function add_text_to_order_item_meta( $item_id, $values ) {

    if ( ! empty( $values['additional_note'] ) ) {

        wc_add_order_item_meta( $item_id, 'Additional notes on order', $values['additional_note'], true );

    }

}


function add_note_on_display_order( $cart_item, $order_item ){

    if( isset( $order_item['additional_note'] ) ){

        $cart_item['additional_note'] = $order_item['additional_note'];

    }

    return $cart_item;

}


//adding additional note in email
function add_note_in_mails( $fields, $order) {

    $fields['additional_note'] = array(
        'label' => __('Label'),
        'value' => get_post_meta($order->id, 'additional_note', true),
    );
    return $fields;

}



 add_action('init', 'register_booking_product_type');
 add_filter( 'woocommerce_product_class', 'booking_product_class', 10, 2 ); 

 add_filter('product_type_selector','add_new_product_type');

 add_filter('woocommerce_product_data_tabs','create_bookable_settings_tab');

 add_action('woocommerce_product_data_panels', 'add_product_panel');

 add_action('woocommerce_process_product_meta', 'save_booking_price');
 

 add_action( 'woocommerce_single_product_summary', 'create_bookable_product_template');
 

 add_filter( 'woocommerce_add_cart_item_data', 'add_to_cart_data', 10, 2 );

 add_filter( 'woocommerce_get_item_data', 'display_on_cart', 10, 2 );

 add_action( 'woocommerce_add_order_item_meta', 'add_text_to_order_item_meta', 10, 2 );

 add_filter( 'woocommerce_order_item_product', 'add_note_on_display_order', 10, 2 );

 add_filter( 'woocommerce_email_order_meta_fields', 'add_note_in_mails',10,2);




 //add_filter( 'woocommerce_cart_item_price', 'get_price_on_cart_page', 10, 3 );
//  add_filter( 'woocommerce_get_price_html', 'show_price', 10, 2 );
?>