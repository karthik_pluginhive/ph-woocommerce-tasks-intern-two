<?php

    global $product;

    do_action('booking_product_before_add_to_cart_form');
   
?>

<form class="booking_product_cart" method='post' enctype='multipart/form-data'>
    <table>
        <tr>
            <td>
                <label for="price">
                    <?php echo __('Total Amount', 'wcpt'); ?>
                </label>
            </td>
            <td class='price'>
                <?php

                    $get_price = get_post_meta($product->get_id(), '_booking_price');
                    $price = 0;
                    
                    if(isset($get_price[0])){
                        $price = wc_price($get_price[0]);
                    }
                    echo $price;
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <label>
                    <?php echo __('Any additional note on your order', 'wcpt'); ?>
                </label>
            </td>
            <td>
                <input name="additional_note">
            </td>
        </tr>
    </table>
    <button type='submit' name='add-to-cart' value="<?php echo esc_attr($product->get_id()); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
</form>

<?php

do_action( 'booking_product_after_add_to_cart_form' );
?>

